-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (armv7l)
--
-- Host: localhost    Database: jasa
-- ------------------------------------------------------
-- Server version	5.5.38-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hub_apps`
--

DROP TABLE IF EXISTS `hub_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_apps` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `app_ID` varchar(45) NOT NULL,
  `version` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `dbs` text NOT NULL,
  `timestamp` datetime NOT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_apps`
--

LOCK TABLES `hub_apps` WRITE;
/*!40000 ALTER TABLE `hub_apps` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_comments`
--

DROP TABLE IF EXISTS `hub_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_comments` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `author_ID` varchar(45) NOT NULL,
  `relative` varchar(255) NOT NULL,
  `content_ID` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `at` varchar(45) NOT NULL,
  `comment` text NOT NULL,
  `timestamp` datetime NOT NULL,
  `rating` int(10) NOT NULL,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_comments`
--

LOCK TABLES `hub_comments` WRITE;
/*!40000 ALTER TABLE `hub_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_encryption`
--

DROP TABLE IF EXISTS `hub_encryption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_encryption` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `enigma` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_encryption`
--

LOCK TABLES `hub_encryption` WRITE;
/*!40000 ALTER TABLE `hub_encryption` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_encryption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_lms_courses`
--

DROP TABLE IF EXISTS `hub_lms_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_lms_courses` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `version` varchar(45) NOT NULL,
  `lore_course_ID` varchar(45) NOT NULL,
  `author` varchar(255) NOT NULL,
  `version_date` datetime NOT NULL,
  `repository` varchar(255) NOT NULL,
  `public` varchar(45) NOT NULL,
  `cover_background` varchar(45) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `price` varchar(45) NOT NULL,
  `language` varchar(255) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_lms_courses`
--

LOCK TABLES `hub_lms_courses` WRITE;
/*!40000 ALTER TABLE `hub_lms_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_lms_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_lms_group`
--

DROP TABLE IF EXISTS `hub_lms_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_lms_group` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `icon` varchar(255) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_lms_group`
--

LOCK TABLES `hub_lms_group` WRITE;
/*!40000 ALTER TABLE `hub_lms_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_lms_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_lms_group_relationship`
--

DROP TABLE IF EXISTS `hub_lms_group_relationship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_lms_group_relationship` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `content_ID` varchar(45) NOT NULL,
  `relative` varchar(45) NOT NULL,
  `group_ID` varchar(45) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_lms_group_relationship`
--

LOCK TABLES `hub_lms_group_relationship` WRITE;
/*!40000 ALTER TABLE `hub_lms_group_relationship` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_lms_group_relationship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_lms_interactions`
--

DROP TABLE IF EXISTS `hub_lms_interactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_lms_interactions` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `score_ID` varchar(45) NOT NULL,
  `game_ID` varchar(45) NOT NULL,
  `question_ID` varchar(45) NOT NULL,
  `question` text NOT NULL,
  `answer_ID` text NOT NULL,
  `answer` text NOT NULL,
  `type` varchar(45) NOT NULL,
  `correct` varchar(45) NOT NULL,
  `attempts` int(11) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=166 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_lms_interactions`
--

LOCK TABLES `hub_lms_interactions` WRITE;
/*!40000 ALTER TABLE `hub_lms_interactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_lms_interactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_lms_memcard`
--

DROP TABLE IF EXISTS `hub_lms_memcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_lms_memcard` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `game_ID` varchar(45) NOT NULL,
  `time_in` varchar(45) NOT NULL,
  `maxScore` int(11) NOT NULL,
  `rawScore` int(11) NOT NULL,
  `game_status` varchar(45) NOT NULL,
  `lore_string` text NOT NULL,
  `suspend_data` varchar(255) NOT NULL,
  `current_index` int(11) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_lms_memcard`
--

LOCK TABLES `hub_lms_memcard` WRITE;
/*!40000 ALTER TABLE `hub_lms_memcard` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_lms_memcard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_lms_module_objects`
--

DROP TABLE IF EXISTS `hub_lms_module_objects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_lms_module_objects` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `course_ID` varchar(45) NOT NULL,
  `objective_ID` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `parent_ID` varchar(45) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `lore_object_ID` varchar(45) NOT NULL,
  `correct` varchar(45) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=551 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_lms_module_objects`
--

LOCK TABLES `hub_lms_module_objects` WRITE;
/*!40000 ALTER TABLE `hub_lms_module_objects` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_lms_module_objects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_lms_modules`
--

DROP TABLE IF EXISTS `hub_lms_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_lms_modules` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `course_ID` varchar(45) NOT NULL,
  `max_score` int(11) NOT NULL,
  `min_score` int(11) NOT NULL,
  `total_score` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `lore_module_ID` varchar(45) NOT NULL,
  `slide_index` int(11) NOT NULL,
  `objective_ID` varchar(45) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=290 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_lms_modules`
--

LOCK TABLES `hub_lms_modules` WRITE;
/*!40000 ALTER TABLE `hub_lms_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_lms_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_lms_organisation`
--

DROP TABLE IF EXISTS `hub_lms_organisation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_lms_organisation` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `title` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `client_ID` varchar(45) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_lms_organisation`
--

LOCK TABLES `hub_lms_organisation` WRITE;
/*!40000 ALTER TABLE `hub_lms_organisation` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_lms_organisation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_lms_posts`
--

DROP TABLE IF EXISTS `hub_lms_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_lms_posts` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `relative` varchar(255) NOT NULL,
  `content_ID` varchar(45) NOT NULL,
  `number` decimal(11,2) NOT NULL,
  `group_ID` varchar(45) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_lms_posts`
--

LOCK TABLES `hub_lms_posts` WRITE;
/*!40000 ALTER TABLE `hub_lms_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_lms_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_lms_scoreboard`
--

DROP TABLE IF EXISTS `hub_lms_scoreboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_lms_scoreboard` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `game_ID` varchar(45) NOT NULL,
  `memcard_ID` varchar(45) NOT NULL,
  `score_ID` varchar(45) NOT NULL,
  `maxScore` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `incorrect` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `mode` varchar(45) NOT NULL,
  `slides` text NOT NULL,
  `status` varchar(45) NOT NULL,
  `progress` int(11) NOT NULL,
  `total_slides` int(11) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_lms_scoreboard`
--

LOCK TABLES `hub_lms_scoreboard` WRITE;
/*!40000 ALTER TABLE `hub_lms_scoreboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_lms_scoreboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_lms_users`
--

DROP TABLE IF EXISTS `hub_lms_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_lms_users` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_number` varchar(45) NOT NULL,
  `org_ID` varchar(45) NOT NULL,
  `race` varchar(45) NOT NULL,
  `first_language` varchar(255) NOT NULL,
  `english_language` varchar(255) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_lms_users`
--

LOCK TABLES `hub_lms_users` WRITE;
/*!40000 ALTER TABLE `hub_lms_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_lms_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_setup`
--

DROP TABLE IF EXISTS `hub_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_setup` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_setup`
--

LOCK TABLES `hub_setup` WRITE;
/*!40000 ALTER TABLE `hub_setup` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_setup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_smtp_queue`
--

DROP TABLE IF EXISTS `hub_smtp_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_smtp_queue` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `priority` varchar(45) NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `to_email` text NOT NULL,
  `subject` text NOT NULL,
  `from_name` text NOT NULL,
  `from_email` text NOT NULL,
  `reply_to` text NOT NULL,
  `email_body` text NOT NULL,
  `email_uri` text NOT NULL,
  `processed` varchar(45) NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_smtp_queue`
--

LOCK TABLES `hub_smtp_queue` WRITE;
/*!40000 ALTER TABLE `hub_smtp_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_smtp_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_uploads`
--

DROP TABLE IF EXISTS `hub_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_uploads` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `filename` text NOT NULL,
  `folder` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `relative` varchar(255) NOT NULL,
  `content_ID` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL,
  `feature` varchar(45) NOT NULL,
  `category` text NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_uploads`
--

LOCK TABLES `hub_uploads` WRITE;
/*!40000 ALTER TABLE `hub_uploads` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_users`
--

DROP TABLE IF EXISTS `hub_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_users` (
  `ID` int(11) NOT NULL,
  `UID` char(36) NOT NULL,
  `ORG_UID` CHAR(36),
  `created_timestamp` datetime NOT NULL,
  `modified_timestamp` datetime NOT NULL,
  `user_ID` varchar(45) NOT NULL,
  `timekey` text NOT NULL,
  `username` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `picture` text NOT NULL,
  `image` text NOT NULL,
  `type` text NOT NULL,
  `region` text NOT NULL,
  `status` text NOT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `about` text NOT NULL,
  `website` text NOT NULL,
  `active` text NOT NULL,
  `student_id` text NOT NULL,
  `timestamp` datetime NOT NULL,
  `joined` datetime NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_users`
--

LOCK TABLES `hub_users` WRITE;
/*!40000 ALTER TABLE `hub_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `hub_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-15 12:22:29
