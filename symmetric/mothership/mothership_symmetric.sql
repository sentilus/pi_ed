delete from sym_trigger_router;
delete from sym_trigger;
delete from sym_router;
delete from sym_channel;
delete from sym_node_group_link;
delete from sym_node_group;
delete from sym_node_host;
delete from sym_node_identity;
delete from sym_node_security;
delete from sym_node;

insert into sym_node_group (node_group_id, description)
    values ('server', 'The central syncing server');

insert into sym_node_group (node_group_id, description)
    values ('pis', 'Raspberry Pis');

insert into sym_node (node_id, node_group_id, external_id, sync_enabled, created_at_node_id)
    values ('mothership', 'server', 'mothership', 1, 'mothership');

insert into sym_node_identity values ('mothership');

insert into sym_node_security (node_id, node_password, registration_enabled, registration_time, initial_load_enabled,
    initial_load_time, created_at_node_id)
    values ('mothership', 'Sp13d2014', 1, current_timestamp, 0, current_timestamp, 'mothership');

insert into sym_node_group_link (source_node_group_id, target_node_group_id, data_event_action)
    values ("pis", 'server', 'P');

insert into sym_node_group_link (source_node_group_id, target_node_group_id, data_event_action)
    values ('server', 'pis', 'W');

insert into sym_router (router_id, source_node_group_id, target_node_group_id, create_time, last_update_time)
    values ('pi-2-server', 'pis', 'server', current_timestamp, current_timestamp);

insert into sym_channel (channel_id, processing_order, max_batch_size, max_batch_to_send, extract_period_millis, batch_algorithm, enabled, description)
    values ('users', 10, 1000, 10, 0, 'default', 1, 'User data');

insert into sym_trigger (trigger_id, source_table_name, channel_id, reload_channel_id, last_update_time, create_time, sync_key_names)
    values ('users', '*', 'users', 'users', current_timestamp, current_timestamp, 'UID');

insert into sym_trigger_router
    (trigger_id, router_id, initial_load_order, create_time, last_update_time)
    values ('users', 'pi-2-server', -1, current_timestamp, current_timestamp);

