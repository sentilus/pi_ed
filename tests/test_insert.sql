truncate table hub_lms_organisation;
insert into hub_lms_organisation
	(created_timestamp, modified_timestamp, user_ID, title, country, province, city, category, website, client_ID)
	values (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'john', 'title', 'country', 'province', 'city', 'category', 'website', 'client_id');

truncate table hub_lms_users;
insert into hub_lms_users
	(created_timestamp, modified_timestamp, user_ID, type, name, surname, dob, email, telephone, gender, mobile, password, id_number, org_ID, race, first_language, english_language)
	values (current_timestamp, current_timestamp, 'john', 'student', 'john', 'seymour', '2015-01-01', 'john@email.com', '012341', 'M', '081352', 'password', '287015', 'org', 'white', 'english', 'english');

truncate table hub_apps;
insert into hub_apps
	(created_timestamp, modified_timestamp, user_ID, app_ID, version, status, dbs, timestamp, type)
	values (current_timestamp, current_timestamp, 'user', 'app', '1', 'done', 'description', current_timestamp, 'appy');

