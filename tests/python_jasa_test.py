from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re


class PythonJasaTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.PhantomJS('phantomjs')
        #self.driver = webdriver.Firefox()

        self.driver.implicitly_wait(30)
        self.base_url = "http://10.0.0.31/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_python_jasa(self):
        name=str(uuid.uuid1())
        surname=str(uuid.uuid1())
        org = str(uuid.uuid1())

        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Register").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys(name)
        driver.find_element_by_id("surname").clear()
        driver.find_element_by_id("surname").send_keys("Mar")
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys(surname)
        driver.find_element_by_id("surname").clear()
        driver.find_element_by_id("surname").send_keys(name)
        driver.find_element_by_id("id_number").clear()
        driver.find_element_by_id("id_number").send_keys("123456")
        Select(driver.find_element_by_id("gender")).select_by_visible_text("Boy")
        driver.find_element_by_link_text("Sign Up").click()
        #self.assertEqual("<h3>Registration Complete</h3><p>Please sign in with your surname and ID number.</p>", self.close_alert_and_get_its_text())
        #driver.find_element_by_id("alert-action").click()
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys(name)
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("123456")
        driver.find_element_by_link_text("Sign-In").click()
        #self.assertEqual("Hi Marvin.  Signing you in please wait.", self.close_alert_and_get_its_text())
        #driver.find_element_by_css_selector("img").click()
        driver.find_element_by_id("field-title").clear()
        driver.find_element_by_id("field-title").send_keys(org)
        driver.find_element_by_link_text("Next Step").click()
        #driver.find_element_by_id("group-1-image").click()
#        driver.find_element_by_name("student").click()
        #driver.find_element_by_id("modalbox-action").click()
        driver.find_element_by_css_selector("div.slide.panel-center > div.column-100 > div.content-box.radius-5 > div.app-feed > div.button-container.text-align-center > a.button").click()
        #self.assertEqual("You need at least 10 people in your group.  You have only added 1", self.close_alert_and_get_its_text())
        driver.find_element_by_id("alert-action").click()
        #driver.find_element_by_id("group-2-image").click()
#        driver.find_element_by_name("student").click()
#        driver.find_element_by_id("modalbox-action").click()
        #driver.find_element_by_id("group-3-image").click()
#        driver.find_element_by_name("student").click()
#        driver.find_element_by_id("modalbox-action").click()
#         driver.find_element_by_id("group-4-image").click()
#         driver.find_element_by_name("student").click()
#         driver.find_element_by_css_selector("#modalbox-action > span").click()
#         driver.find_element_by_id("group-5-image").click()
#         driver.find_element_by_name("student").click()
#         driver.find_element_by_id("modalbox-action").click()
#         driver.find_element_by_id("group-6-image").click()
#         driver.find_element_by_name("student").click()
#         driver.find_element_by_css_selector("#modalbox-action > span").click()
#         driver.find_element_by_id("group-7-image").click()
#         driver.find_element_by_name("student").click()
#         driver.find_element_by_css_selector("#modalbox-action > span").click()
#         driver.find_element_by_id("group-8-image").click()
#         driver.find_element_by_name("student").click()
#         driver.find_element_by_css_selector("#modalbox-action > span").click()
#         driver.find_element_by_id("group-9-image").click()
#         driver.find_element_by_name("student").click()
#         driver.find_element_by_css_selector("#modalbox-action > span").click()
#         driver.find_element_by_id("group-10-image").click()
#         driver.find_element_by_name("student").click()
#         driver.find_element_by_css_selector("#modalbox-action > span").click()
        driver.find_element_by_css_selector("div.slide.panel-center > div.column-100 > div.content-box.radius-5 > div.app-feed > div.button-container.text-align-center > a.button").click()
        driver.find_element_by_link_text("Yes, Save").click()
        #self.assertEqual("<div class=\"text-align-center\"> 					<div class=\"company-icon icon-64x64\" style=\"margin:15px auto\"></div> 					<h2>A New Company Created</h2> 					<p>Well done team! Your company is now created.  You can now go back to e-learning and continue your journey.</p> 				</div>", self.close_alert_and_get_its_text())
        driver.find_element_by_id("alert-action").click()
        # Select(driver.find_element_by_css_selector("select")).select_by_visible_text("Business Ethics Statement")
        # driver.find_element_by_css_selector("option[value=\"2\"]").click()
        # driver.find_element_by_link_text("+Statement").click()
        # driver.find_element_by_id("field-content").clear()
        # driver.find_element_by_id("field-content").send_keys("To make sure everything runs smoothly")
        # driver.find_element_by_css_selector("div.menu-icon").click()
        # driver.find_element_by_link_text("e-Learning").click()
        # driver.close()


    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to.alert
        except NoAlertPresentException: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()


