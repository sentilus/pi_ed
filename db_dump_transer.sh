#!/bin/bash

REMOTE="10.0.0.15"
REMOTE_USER="root"
REMOTE_FOLDER="/home/piEd/jasa"
REMOTE_SCRIPT="/root/scripts/import_db.sh"
MYSQL_PASS="password"

chmod a+rwx /tmp/jasa/*
rm /tmp/jasa/*.txt
echo Dumping database...
mysqldump -u root --password="${MYSQL_PASS}" -t --no-create-info --tab=/tmp/jasa jasa
chmod a+rwx /tmp/jasa/*
echo Remote syncing...
rsync -vz /tmp/jasa/*.txt $REMOTE_USER@$REMOTE:$REMOTE_FOLDER
echo Running remote insert script...
ssh $REMOTE_USER@10.0.0.15 sh $REMOTE_SCRIPT
echo Cleaning up...
rm /tmp/jasa/*.txt