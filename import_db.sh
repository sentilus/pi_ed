#!/bin/bash

ROOT_PASS="password"
DUMP_FOLDER="/home/piEd/jasa"

table_names=`mysql -u root --password="${ROOT_PASS}" -s -N -e "show tables in jasa;"`
for name in $table_names; do 
        dumpname="${DUMP_FOLDER}/${name}.txt"
        echo $dumpname
        mysqlimport -u root --password="${ROOT_PASS}" --local jasa ${dumpname};
done
